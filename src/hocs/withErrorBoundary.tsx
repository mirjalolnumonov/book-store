import React, { Component } from "react";

const styles = {
  error: {
    backgroundColor: "#f98e7e",
    borderTop: "1px solid #777",
    borderBottom: "1px solid #777",
    padding: "12px",
  },
};

const withErrorBoundary = (WrappedComponent: any) =>
  class extends Component {
    state = { error: null, errorInfo: null };

    componentDidCatch = (error: any, errorInfo: any) => {
      this.setState({
        error: error,
        errorInfo: errorInfo,
      });
    };



    render() {
      console.log("in boundary");
      if (this.state.errorInfo) {
        return (
          <>
            <div style={styles.error}>
              <h2>Something went wrong.</h2>
              <div>{this.state.errorInfo}</div>
            </div>
          </>
        );
      }
      // Normally, just render children
      return <WrappedComponent {...this.props} />;
    }
  };

export default withErrorBoundary;
