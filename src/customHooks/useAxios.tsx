import { useState, useEffect } from 'react';
import axios from 'axios';
import * as CONSTANTS from '../config/urls/urls'

axios.defaults.baseURL = CONSTANTS.F_BASE;

const useAxios = ({ url = CONSTANTS.F_BASE, method = "get", body = "", headers = "" }) => {
    const [response, setResponse] = useState(null);
    const [error, setError] = useState('');
    const [loading, setloading] = useState(true);

    const fetchData = () => {
        axios.post(url, JSON.parse(headers), JSON.parse(body))
            .then((res:any) => {
                setResponse(res.data);
            })
            .catch((err:any) => {
                setError(err);
            })
            .finally(() => {
                setloading(false);
            });
    };

    useEffect(() => {
        fetchData();
    }, [method, url, body, headers]);

    return { response, error, loading };
};

export default useAxios;