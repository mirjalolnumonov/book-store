import React from "react";
import { Container, Divider } from "@material-ui/core";
import TopMenuItems from "../../components/Header/TopMenu/TopMenuItems/TopMenuItems";
import TopMain from "../../components/Header/TopMain/TopMain";
import FooterPanel from "../../components/Footer/FooterPanel/FooterPanel";
import NotFound from "../../components/NotFound/NotFound";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import BookList from "../../components/BookList/BookList";
import BookDetail from "../../components/BookDetail/BookDetail";
import classes from "./Home.module.scss";
const HomeContainer: React.FC = (): JSX.Element => {
  let { path, url } = useRouteMatch();
  return (
    <Container maxWidth="lg">
      <TopMenuItems />
      <TopMain />
      <Divider />
      <div className={classes.Main}>
        <Switch>
          <Route path="/" exact>
            <BookList />
          </Route>
          <Route path="/book/:topicId">
            <BookDetail />
          </Route>
          <Route path="*" component={NotFound} />
        </Switch>
      </div>
      <Divider />
      <FooterPanel />
    </Container>
  );
};

export default HomeContainer;
