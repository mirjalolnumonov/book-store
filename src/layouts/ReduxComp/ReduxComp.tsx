import { connect } from "react-redux";
import * as actionTypes from "../../store/actions/index";
import * as actionTypesCon from "../../store/actions/actionTypes";


const ReduxComp = (props:any) => {
    return (
        <div className="App">
        {
          props.result.map((el:any, index:number) => {
            return <li onClick = {()=>props.OnDeleteResult(index)} key={index}>{el}</li>
          })
        }
        <button onClick={()=>props.OnIncrementCounter(props.ctr)}>Add 10</button>
        <button onClick={()=>props.OnDecrementCounter(props.ctr)}>Minus 10</button>
        <button onClick={()=>props.OnAddResult(props.ctr)}>Add result</button>
    </div>
    )
}


const mapStateToProps = (state:any) => {
    return {
      ctr: state.ctr.value,
      result: state.res.result
    }
  }
  
  const mapDispatchToProps = (dispatch:any) => {
    return {
      OnIncrementCounter: (v:any)=>dispatch({type: actionTypesCon.INCREMENT, value: v, val:10}),
      OnDecrementCounter: (v:any)=>dispatch(actionTypes.decrement(v, 10)),
      OnAddResult: (val:any)=>dispatch(actionTypes.storeResult(val)),
      OnDeleteResult: (id:any)=>dispatch(actionTypes.deletItem(id))
    }
  }

  export default connect(mapStateToProps, mapDispatchToProps)(ReduxComp)
  