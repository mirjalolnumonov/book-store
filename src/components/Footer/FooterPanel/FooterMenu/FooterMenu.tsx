import { Menu, MenuList, MenuItem } from "@material-ui/core";
import React from "react";
const FooterMenu: React.FC = () => {
  return (
    <MenuList>
      <MenuItem>Contacts</MenuItem>
      <MenuItem>About</MenuItem>
      <MenuItem>Deliver</MenuItem>
      <MenuItem>Discounts</MenuItem>
      <MenuItem>Terms and conditions</MenuItem>
      <MenuItem>Dealers</MenuItem>
      <MenuItem>Recomended</MenuItem>
    </MenuList>
  );
};

export default FooterMenu;
