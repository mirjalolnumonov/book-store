import { Grid } from "@material-ui/core";
import React from "react";
import FooterMenu from "./FooterMenu/FooterMenu";
import SocialNetworks from "./SocialNetworks/SocialNetworks";
const FooterPanel: React.FC = () => {
  return (
    <Grid container item xs={12}>
      <Grid item xs={3}>
        <FooterMenu />
      </Grid>
      <Grid item xs={3}>
        <FooterMenu />
      </Grid>
      <Grid item xs={3}>
        <FooterMenu />
      </Grid>
      <Grid item xs={3}>
        <SocialNetworks />
      </Grid>
    </Grid>
  );
};

export default FooterPanel;
