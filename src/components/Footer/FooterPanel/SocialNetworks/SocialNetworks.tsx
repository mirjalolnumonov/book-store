import React from "react";
import { Grid } from "@material-ui/core";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import TwitterIcon from "@material-ui/icons/Twitter";
import RedditIcon from "@material-ui/icons/Reddit";
import PhoneIcon from "@material-ui/icons/Phone";
import EmailIcon from "@material-ui/icons/Email";
import PinterestIcon from "@material-ui/icons/Pinterest";
import classes from "./SocialNetworks.module.scss";
const SocialNetworks: React.FC = () => {
  return (
    <Grid className={classes.Wrapper} container item xs={12}>
      <Grid item xs={12}>
        <p>
          <PhoneIcon />
          <b> +99890 789-45-56</b>
        </p>
      </Grid>
      <Grid item xs={12}>
        <p>
          <PhoneIcon />
          <b> +99890 789-78-90</b>
        </p>
      </Grid>
      <Grid item xs={12}>
        <p>
          <EmailIcon />
          <b> bookstore@gmail.com</b>
        </p>
        <br />
      </Grid>
      <Grid item xs={4}>
        <FacebookIcon />
      </Grid>
      <Grid item xs={4}>
        <InstagramIcon />
      </Grid>
      <Grid item xs={4}>
        <LinkedInIcon />
      </Grid>
      <Grid item xs={4}>
        <TwitterIcon />
      </Grid>
      <Grid item xs={4}>
        <RedditIcon />
      </Grid>
      <Grid item xs={4}>
        <PinterestIcon />
      </Grid>
    </Grid>
  );
};

export default SocialNetworks;
