import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Slider from "@material-ui/core/Slider";

import { withStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import Favorite from "@material-ui/icons/Favorite";
import FavoriteBorder from "@material-ui/icons/FavoriteBorder";
import { Button, Grid } from "@material-ui/core";

const GreenCheckbox = withStyles({
  root: {
    color: green[400],
    "&$checked": {
      color: green[600],
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

const useStyles = makeStyles((theme) => ({
  root: {
    width: "90%",
    marginTop: "20px",
    paddingRight: "15px",
    borderRight: "1px solid lightgray",
    minHeight: "1400px",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

const useStylesSlider = makeStyles((theme) => ({
  root: {
    width: 250,
  },
  margin: {
    height: theme.spacing(3),
  },
}));

const marks = [
  {
    value: 0,
    label: "$0",
  },
  {
    value: 10,
    label: "$10",
  },
  {
    value: 30,
    label: "$30",
  },
  {
    value: 60,
    label: "$60",
  },
  {
    value: 100,
    label: "100$",
  },
];

function valuetext(value: number) {
  return `$${value}`;
}

const Filter: React.FC = () => {
  const [state, setState] = React.useState({
    checkedA: false,
    checkedB: false,
    checkedF: false,
    checkedG: false,
  });

  const handleChange = (event: any) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const classes = useStyles();
  const classesSlider = useStylesSlider();

  return (
    <div className={classes.root}>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>Price</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <div className={classesSlider.root}>
            <Slider
              track="inverted"
              aria-labelledby="track-inverted-range-slider"
              getAriaValueText={valuetext}
              defaultValue={[0, 40]}
              marks={marks}
            />
          </div>
        </AccordionDetails>
      </Accordion>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography className={classes.heading}>Year</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <FormGroup row>
            <Grid lg={12} container justifyContent="flex-start">
              <FormControlLabel
                control={
                  <Checkbox
                    checked={state.checkedA}
                    onChange={handleChange}
                    name="checkedA"
                    color="primary"
                  />
                }
                label="2015"
              />
            </Grid>
            <Grid lg={12} container justifyContent="flex-start">
              <FormControlLabel
                control={
                  <Checkbox
                    checked={state.checkedB}
                    onChange={handleChange}
                    name="checkedB"
                    color="primary"
                  />
                }
                label="2016"
              />
            </Grid>
            <Grid lg={12} container justifyContent="flex-start">
              <FormControlLabel
                control={
                  <Checkbox
                    checked={state.checkedF}
                    onChange={handleChange}
                    name="checkedF"
                    color="primary"
                  />
                }
                label="2017"
              />
            </Grid>
            <Grid lg={12} container justifyContent="flex-start">
              <FormControlLabel
                control={
                  <Checkbox
                    checked={state.checkedG}
                    onChange={handleChange}
                    name="checkedG"
                    color="primary"
                  />
                }
                label="2018"
              />
            </Grid>
          </FormGroup>
        </AccordionDetails>
      </Accordion>
      <Button variant="contained" color="primary" style={{width:'100%'}}>Filter</Button>
    </div>
  );
};

export default Filter;
