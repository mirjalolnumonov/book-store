import { Grid } from "@material-ui/core";
import React from "react";
import CardS from "./Card/Card";
import Filter from "./Filter/Filter";
import PageCount from "./PageCount/PageCount";
import Pagination from "./Pagination/Pagination";
import Sorter from "./Sorter/Sorter";

const BookList: React.FC = () => {

    let items = [];
    for(let i = 0; i < 9; i++){
        items[i] = <CardS key={i} />
    }

  return (
    <Grid container lg={12}>
      <Grid item lg={3}>
        <Filter />
      </Grid>
      <Grid item lg={9} wrap={'wrap'}>
        <Grid container  lg={12}>
          <Grid container item lg={6}>
            <Sorter />
          </Grid>
          <Grid container justifyContent='flex-end' lg={6} spacing={1}>
            <PageCount />
          </Grid>
        </Grid>
        <Grid container lg={12} justifyContent="space-between" spacing={1}>
                {
                    items
                }
        </Grid>
        <Grid container justifyContent={'center'} lg={12} >
            <Pagination />
        </Grid>
      </Grid>
    </Grid>
  )
};

export default BookList;
