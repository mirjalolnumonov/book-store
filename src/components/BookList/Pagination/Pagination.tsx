import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Pagination from '@material-ui/lab/Pagination';

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      marginTop: theme.spacing(5),
      marginBottom: theme.spacing(5),
      textAlign: 'center'
    },
  },
  container: {
    justifyContent: "center"
  }
}));

const PaginationS: React.FC = () => {
  const classes = useStyles();
  return (
    <div className={`${classes.root} ${classes.container}`}>
      <Pagination count={10} showFirstButton showLastButton />
    </div>
  );
};

export default PaginationS;
