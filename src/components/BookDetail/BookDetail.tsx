import { Button, Grid } from "@material-ui/core";
import React from "react";
import book_1 from "../../assets/images/books/book_1.jpg";
import classes from "./BookDetail.module.scss"
const BookDetail: React.FC = () => {
  return (
    <Grid container >
      <Grid container xs={3} style={{alignContent: "flex-start"}}>
        <img className={classes.Image} src={book_1} alt="" />
      </Grid>
      <Grid container xs={9} className={classes.Content}>
        <Grid item xs={12}>
          <Grid container xs={12} className={classes.ContentTop}>
            <Grid item xs={3}>
              <b>Name: Eloquent Javascript</b>
            </Grid>
            <Grid item xs={3}>
              <b>Price: $25</b>
            </Grid>
            <Grid item xs={3}>
              <b>Year: 2018</b>
            </Grid>
            <Grid item xs={3}>
              <Button variant="outlined" color="primary">Add to cart</Button>
            </Grid>
          </Grid>
        </Grid>
        <Grid item justifyContent={"space-between"} xs={12}>
            <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque
          aliquam doloribus, nulla, voluptatem, velit ullam reiciendis rerum
          consequatur ratione perspiciatis assumenda sunt vel animi facilis
          autem nihil adipisci cumque vitae. Lorem, ipsum dolor sit amet
          consectetur adipisicing elit. Quas, quasi laboriosam repellendus
          tempore ex labore sapiente omnis aliquam debitis voluptas perspiciatis
          deserunt porro earum fuga, voluptatem hic quis, blanditiis
          accusantium!
          </p>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default BookDetail;
