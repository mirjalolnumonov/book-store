import { FC, useState } from "react";
import { useForm } from "../../../customHooks/useForm";
import { Button, CircularProgress, TextField } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { Alert } from "@material-ui/lab";
import axios from "axios";
import withErrorBoundary from "../../../hocs/withErrorBoundary";

interface Category {
  name: string;
  name_ru: string;
  name_en: string;
}

const AddCategory: FC = () => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState("");
  const [loading, setloading] = useState(false);
  const { t } = useTranslation();
  const { handleSubmit, handleChange, data, errors, clearForm } =
    useForm<Category>({
      validations: {
        name: {
          custom: {
            isValid: (value) => value.length > 3,
            message: t("errors.cat_name_error"),
          },
        },
        name_ru: {
          custom: {
            isValid: (value) => value.length > 3,
            message: t("errors.cat_name_ru_error"),
          },
        },
        name_en: {
          custom: {
            isValid: (value) => value.length > 3,
            message: t("errors.cat_name_en_error"),
          },
        },
      },
      onSubmit: () => AddElement(),
    });

  const AddElement = async () => {
    setloading(true);

    await axios
      .post(
        "https://bookstore-fa5e2-default-rtdb.firebaseio.com/categories.json",
        data
      )
      .then((res: any) => {
        setResponse(res.data);
        clearForm();
      })
      .catch((err: any) => {
          console.log(err.message);
          console.log(typeof err);
        setError(err.message);
      })
      .finally(() => {
        setloading(false);
      });
  };
  let errorMessage = [];
  if (errors.name) {
    let el = (
      <>
        <Alert severity="error">{errors.name}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.name_en) {
    let el = (
      <>
        <Alert severity="error">{errors.name_en}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.name_ru) {
    let el = (
      <>
        <Alert severity="error">{errors.name_ru}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  if (error) {
    let el = (
      <>
        <Alert severity="error">{error}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  let load = null;
  if (loading) {
    load = (
      <>
        {" "}
        <CircularProgress />
        <br />
      </>
    );
  }

  let success = null;
  if (response) {
    success = (
      <>
        <Alert severity="success">{t("category_added")}</Alert>
        <br />
      </>
    );
  }
  return (
    <>
      <p>{t("add_category")}</p>
      {load}
      {success}
      {errorMessage.map((el) => {
        return el;
      })}
      <form action="/" method="post" onSubmit={handleSubmit}>
        <TextField
          name="name"
          label={t("code")}
          variant="outlined"
          onChange={handleChange("name")}
          required
          value={data.name || ""}
        />
        <br />
        <br />
        <TextField
          name="name_en"
          label={t("name_en")}
          variant="outlined"
          onChange={handleChange("name_en")}
          required
          value={data.name_en || ""}
        />
        <br />
        <br />
        <TextField
          name="name_ru"
          label={t("name_ru")}
          variant="outlined"
          onChange={handleChange("name_ru")}
          required
          value={data.name_ru || ""}
        />
        <br />
        <br />
        <Button type="submit" variant="contained" color="primary">
          {t("send")}
        </Button>
      </form>
    </>
  );
};
export default withErrorBoundary(AddCategory);
