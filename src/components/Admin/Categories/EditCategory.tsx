import { FC, useState } from "react";
import { useForm } from "../../../customHooks/useForm";
import { Button, CircularProgress, TextField } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { Alert } from "@material-ui/lab";
import axios from "axios";
import withErrorBoundary from "../../../hocs/withErrorBoundary";
import { Redirect, withRouter } from "react-router-dom";
import * as Routes from "../../../config/routes/routes";
import { connect, useSelector } from "react-redux";

interface Category {
  name: string;
  name_ru: string;
  name_en: string;
}

const EditCategory: FC = (props:any) => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState("");
  const [loading, setloading] = useState(false);
  const { t } = useTranslation();
  const category = useSelector((state: any) => state.ctg.category);

  let { handleSubmit, handleChange, data, errors, clearForm } =
    useForm<Category>({
      validations: {
        name: {
          custom: {
            isValid: (value) => value.length > 3,
            message: t("errors.cat_name_error"),
          },
        },
        name_ru: {
          custom: {
            isValid: (value) => value.length > 3,
            message: t("errors.cat_name_ru_error"),
          },
        },
        name_en: {
          custom: {
            isValid: (value) => value.length > 3,
            message: t("errors.cat_name_en_error"),
          },
        },
      },
      onSubmit: ()=>EditElement(props),
      initialValues: category,
    });


   

  const EditElement = async (props:any) => {
    setloading(true);
    await axios
      .put(
        `https://bookstore-fa5e2-default-rtdb.firebaseio.com/categories/${category.identifier}.json`,
        data
      )
      .then((res: any) => {
        setResponse(res.data);
        setTimeout(()=>{
            props.history.push(Routes.D_CATEGORIES); 
        }, 2000);
       })
      .catch((err: any) => {
        setError(err.message);
      })
      .finally(() => {
        setloading(false);
      });
  };
  let errorMessage = [];
  if (errors.name) {
    let el = (
      <>
        <Alert severity="error">{errors.name}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.name_en) {
    let el = (
      <>
        <Alert severity="error">{errors.name_en}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.name_ru) {
    let el = (
      <>
        <Alert severity="error">{errors.name_ru}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  console.log(error);
  if (error) {
    let el = (
      <>
        <Alert severity="error">{error}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  let load = null;
  if (loading) {
    load = (
      <>
        {" "}
        <CircularProgress />
        <br />
      </>
    );
  }

  let success = null;
  if (response) {
    success = (
      <>
        <Alert severity="success">{t("category_changed")}</Alert>
        <br />
      </>
    );
  }

  if(!category.name){
    props.history.push(Routes.D_CATEGORIES); 
  }

  return (
    <>
      <p>{t("edit_category")}</p>
      {load}
      {success}
      {errorMessage.map((el) => {
        return el;
      })}
      <form action="/" method="post" onSubmit={handleSubmit}>
        <TextField
          name="name"
          label={t("code")}
          variant="outlined"
          onChange={handleChange("name")}
          required
          value={data.name || ""}
        />
        <br />
        <br />
        <TextField
          name="name_en"
          label={t("name_en")}
          variant="outlined"
          onChange={handleChange("name_en")}
          required
          value={data.name_en || ""}
        />
        <br />
        <br />
        <TextField
          name="name_ru"
          label={t("name_ru")}
          variant="outlined"
          onChange={handleChange("name_ru")}
          required
          value={data.name_ru || ""}
        />
        <br />
        <br />
        <Button type="submit" variant="contained" color="primary">
          {t("edit")}
        </Button>
      </form>
    </>
  );
};

const mapStateToProps = (state: any) => {
  return {
    ctg: state.category,
  };
};
export default connect(mapStateToProps)(withErrorBoundary(withRouter(EditCategory)));
