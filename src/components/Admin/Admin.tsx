import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Drawer from "@material-ui/core/Drawer";
import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Link from "@material-ui/core/Link";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import { Route, Switch, Link as LinkC } from "react-router-dom";
import { MainListItems } from "./LeftMenu/LeftMenuItems/LeftMenuItems";
import Orders from "./Orders/Orders";
import Categories from "./Categories/ViewCategory";
import Books from "./Books/ViewBooks";
import Socials from "./SocialNetworks/ViewSocialNetworks";
import EditSettings from "./Settings/EditSettings/EditSettings";
import ViewProfile from "./Settings/ViewProfile/ViewProfile";
import SwitchLang from "../Header/TopMain/TopLeftPanel/SwitchLang/SwitchLang";
import classesS from './Admin.module.scss'
import  * as Routes from "../../config/routes/routes"
import { useTranslation } from "react-i18next";
import AddCategory from "./Categories/AddCategory";
import EditCategory from "./Categories/EditCategory";
import AddSocialNetwork from "./SocialNetworks/AddSocialNetwork";
import EditSocialNetwork from "./SocialNetworks/EditSocialNetwork";
import AddBook from "./Books/AddBook";
import EditBook from "./Books/EditBook";
import ViewUsers from "./Users/ViewUsers";
import EditUser from "./Users/EditUser";
import AddUser from "./Users/AddUser";
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="#">
        Book Store
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: "none",
  },
  title: {
    flexGrow: 1,
    color: "white",
    textDecoration: "none",
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
}));

export default function Dashboard() {
  const { t } = useTranslation();
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
    <div className={classes.root}>
      <CssBaseline />

      <AppBar
        position="absolute"
        className={clsx(classes.appBar, open && classes.appBarShift)}
      >
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(
              classes.menuButton,
              open && classes.menuButtonHidden
            )}
          >
            {" "}
           
            <MenuIcon />
            
          </IconButton>
          <LinkC to={Routes.ROOT} className={classesS.Link}>
            <Typography
              component="h1"
              variant="h6"
              color="secondary"
              noWrap
              className={classes.title}
            >
              {t("dashboard")}
            </Typography>
            </LinkC>
            <Typography
              component="h1"
              variant="h6"
              color="secondary"
              noWrap
              className={classesS.Lang}
            >
              <SwitchLang />
            </Typography>
         
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>
          <MainListItems />
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={3}>
            {/* Recent Orders */}
            <Grid item xs={12}>
              <Paper className={classes.paper}>
                <Switch>
                  <Route path={Routes.D_USERS}>
                    <ViewUsers />
                  </Route>
                  <Route path={Routes.D_ADD_USER}>
                    <AddUser />
                  </Route>
                  <Route path={Routes.D_EDIT_USER}>
                    <EditUser />
                  </Route>
                  <Route path={Routes.D_CATEGORIES}>
                    <Categories />
                  </Route>
                  <Route path={Routes.D_ADD_CATEGORIES}>
                    <AddCategory />
                  </Route>
                  <Route path={Routes.D_EDIT_CATEGORY}>
                    <EditCategory />
                  </Route>
                  <Route path={Routes.D_BOOKS}>
                    <Books />
                  </Route>
                  <Route path={Routes.D_ADD_BOOK}>
                    <AddBook />
                  </Route>
                  <Route path={Routes.D_EDIT_BOOK}>
                    <EditBook />
                  </Route>
                  <Route path={Routes.D_ORDERS}>
                    <Orders />
                  </Route>
                  
                  {/* social networks */}

                  <Route path={Routes.D_SOCIALS}>
                    <Socials />
                  </Route>
                  <Route path={Routes.D_ADD_SOCIAL}>
                    <AddSocialNetwork />
                  </Route>
                  <Route path={Routes.D_EDIT_SOCIAL}>
                    <EditSocialNetwork />
                  </Route>

                  <Route path={Routes.D_EDIT_PROFILE}>
                    <EditSettings />
                  </Route>
                  <Route path={Routes.DASHBOARD}>
                    <ViewProfile/>
                  </Route>
                </Switch>
              </Paper>
            </Grid>
          </Grid>
          <Box pt={4}>
            <Copyright />
          </Box>
        </Container>
      </main>
    </div>
  );
}
