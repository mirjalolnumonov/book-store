import classes from "./ViewProfile.module.scss";
import { Box, Button, Grid, Paper, TextField } from "@material-ui/core";
import React from "react";
import image from "../../../../assets/images/man.jpg";
import { makeStyles } from "@material-ui/core/styles";
import {useRouteMatch, Link} from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    ...theme.typography.button,
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(1),
  },
}));

const ViewProfile: React.FC = () => {
  let { path, url } = useRouteMatch();
  const klasses = useStyles();
  return (
    <Grid container lg={12} spacing={3}>
      <Grid item lg={4}>
        <img className={classes.Image} src={image} alt="Profile pic" />
      </Grid>
      <Grid item lg={8}>
        <div className={klasses.root}>
            <ul className={classes.List}>
              <li>Name: Mirjalol Numonov</li>
              <li>Email: mirjalolnumonov@gmail.com</li>
              <li>About: Lorem ipsum dolor sit amet 
                consectetur adipisicing elit. Reiciendis 
                totam illo officiis nesciunt, minus tempore 
                numquam iste nemo ab eligendi, ad, et ea at 
                laudantium eaque deleniti maiores dolore exercitationem?</li>
            </ul>
            <Link to={`${url}/editprofile`}><Button variant="contained" color="primary">Edit</Button></Link>
        </div>
      </Grid>
    </Grid>
  );
};

export default ViewProfile;
