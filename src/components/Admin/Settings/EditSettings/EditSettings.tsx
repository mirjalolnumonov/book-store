import React from "react";
import classes from "./EditSettings.module.scss";
import { Button, Grid, TextareaAutosize, TextField } from "@material-ui/core";
import image from "../../../../assets/images/man.jpg";
import { makeStyles } from "@material-ui/core/styles";
import { useRouteMatch, Link } from "react-router-dom";
import FileUploadComponent from "../../SharedComponents/ImageUploader/ImageUploader";

const useStyles = makeStyles((theme) => ({
  root: {
    ...theme.typography.button,
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(1),
  },
}));

const EditSettings: React.FC = () => {
  let { path, url } = useRouteMatch();
  const klasses = useStyles();
  return (
    <Grid container lg={12} spacing={3}>
      <Grid item lg={4}>
        <img className={classes.Image} src={image} alt="Profile pic" />
      </Grid>
      <Grid item lg={8}>
        <div className={klasses.root}>
          <ul className={classes.List}>
            <li>
              Name:{" "}
              <TextField style={{ width: "100%" }} value="Mirjalol Numonov" />
            </li>
            <li>
              Email:{" "}
              <TextField
                style={{ width: "100%" }}
                value="mirjalolnumonov@gmail.com"
              />
            </li>
            <li>
              <textarea rows={10} cols={88}>
                About: Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Reiciendis totam illo officiis nesciunt, minus tempore numquam
                iste nemo ab eligendi, ad, et ea at laudantium eaque deleniti
                maiores dolore exercitationem?"
              </textarea>
            </li>
            <li>
              <FileUploadComponent />
            </li>
          </ul>

          <Button variant="contained" color="primary">
            Save
          </Button>
        </div>
      </Grid>
    </Grid>
  );
};

export default EditSettings;
