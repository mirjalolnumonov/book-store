import { FC, useState } from "react";
import { useForm } from "../../../customHooks/useForm";
import { Button, CircularProgress, TextField } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { Alert } from "@material-ui/lab";
import axios from "axios";
import withErrorBoundary from "../../../hocs/withErrorBoundary";

interface Social {
  icon: string;
  link: string;
}

const AddSocial: FC = () => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState("");
  const [loading, setloading] = useState(false);
  const { t } = useTranslation();
  const { handleSubmit, handleChange, data, errors, clearForm } =
    useForm<Social>({
      validations: {
        icon: {
          custom: {
            isValid: (value) => value.length > 3,
            message: t("errors.social_icon_error"),
          },
        },
        link: {
          custom: {
            isValid: (value) => value.length > 10,
            message: t("errors.social_link_error"),
          },
        }
      },
      onSubmit: () => AddSocial(),
    });

  const AddSocial = async () => {
    setloading(true);

    await axios
      .post(
        "https://bookstore-fa5e2-default-rtdb.firebaseio.com/socials.json",
        data
      )
      .then((res: any) => {
        setResponse(res.data);
        clearForm();
        setError("");
      })
      .catch((err: any) => {
        setError(err.message);
      })
      .finally(() => {
        setloading(false);
      });
  };
  let errorMessage = [];
  if (errors.icon) {
    let el = (
      <>
        <Alert severity="error">{errors.icon}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.link) {
    let el = (
      <>
        <Alert severity="error">{errors.link}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  if (error) {
    let el = (
      <>
        <Alert severity="error">{error}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  let load = null;
  if (loading) {
    load = (
      <>
        {" "}
        <CircularProgress />
        <br />
      </>
    );
  }

  let success = null;
  if (response) {
    success = (
      <>
        <Alert severity="success">{t("social_added")}</Alert>
        <br />
      </>
    );
  }
  return (
    <>
      <p>{t("add_social")}</p>
      {load}
      {success}
      {errorMessage.map((el) => {
        return el;
      })}
      <form action="/" method="post" onSubmit={handleSubmit}>
        <TextField
          name="icon"
          label={t("code")}
          variant="outlined"
          onChange={handleChange("icon")}
          required
          value={data.icon || ""}
        />
        <br />
        <br />
        <TextField
          name="link"
          label={t("link")}
          variant="outlined"
          onChange={handleChange("link")}
          required
          value={data.link || ""}
        />
        <br />
        <br />
        <Button type="submit" variant="contained" color="primary">
          {t("send")}
        </Button>
      </form>
    </>
  );
};
export default withErrorBoundary(AddSocial);
