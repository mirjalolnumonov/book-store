import { FC, useState } from "react";
import { useForm } from "../../../customHooks/useForm";
import { Button, CircularProgress, TextField } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { Alert } from "@material-ui/lab";
import axios from "axios";
import withErrorBoundary from "../../../hocs/withErrorBoundary";
import { Redirect, withRouter } from "react-router-dom";
import * as Routes from "../../../config/routes/routes";
import { connect, useSelector } from "react-redux";

interface Social {
  icon: string;
  link: string;
}

const EditCategory: FC = (props:any) => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState("");
  const [loading, setloading] = useState(false);
  const { t } = useTranslation();
  const social = useSelector((state: any) => state.scl.social);

  let { handleSubmit, handleChange, data, errors, clearForm } =
    useForm<Social>({
      validations: {
        icon: {
          custom: {
            isValid: (value) => value.length > 3,
            message: t("errors.social_icon_error"),
          },
        },
        link: {
          custom: {
            isValid: (value) => value.length > 10,
            message: t("errors.social_link_error"),
          },
        }
      },
      onSubmit: ()=>EditSocial(props),
      initialValues: social,
    });


   

  const EditSocial = async (props:any) => {
    setloading(true);
    await axios
      .put(
        `https://bookstore-fa5e2-default-rtdb.firebaseio.com/socials/${social.identifier}.json`,
        data
      )
      .then((res: any) => {
        setResponse(res.data);
        setTimeout(()=>{
            props.history.push(Routes.D_SOCIALS); 
        }, 2000);
       })
      .catch((err: any) => {
        setError(err.message);
      })
      .finally(() => {
        setloading(false);
      });
  };
  let errorMessage = [];
  if (errors.icon) {
    let el = (
      <>
        <Alert severity="error">{errors.icon}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.link) {
    let el = (
      <>
        <Alert severity="error">{errors.link}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (error) {
    let el = (
      <>
        <Alert severity="error">{error}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  let load = null;
  if (loading) {
    load = (
      <>
        {" "}
        <CircularProgress />
        <br />
      </>
    );
  }

  let success = null;
  if (response) {
    success = (
      <>
        <Alert severity="success">{t("social_changed")}</Alert>
        <br />
      </>
    );
  }

  if(!social.icon){
    props.history.push(Routes.D_SOCIALS); 
  }

  return (
    <>
      <p>{t("edit_social")}</p>
      {load}
      {success}
      {errorMessage.map((el) => {
        return el;
      })}
      <form action="/" method="post" onSubmit={handleSubmit}>
        <TextField
          name="icon"
          label={t("icon")}
          variant="outlined"
          onChange={handleChange("icon")}
          required
          value={data.icon || ""}
        />
        <br />
        <br />
        <TextField
          name="link"
          label={t("link")}
          variant="outlined"
          onChange={handleChange("link")}
          required
          value={data.link || ""}
        />
        <br />
        <br />
        <Button type="submit" variant="contained" color="primary">
          {t("edit")}
        </Button>
      </form>
    </>
  );
};

const mapStateToProps = (state: any) => {
  return {
    ctg: state.category,
  };
};
export default connect(mapStateToProps)(withErrorBoundary(withRouter(EditCategory)));
