import { FC, useState } from "react";
import { useForm } from "../../../customHooks/useForm";
import {
  Button,
  CircularProgress,
  FormControlLabel,
  Grid,
  TextField,
} from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { Alert } from "@material-ui/lab";
import axios from "axios";
import withErrorBoundary from "../../../hocs/withErrorBoundary";
import Checkbox from "@material-ui/core/Checkbox";
import { validateEmail } from "../../../utils/utils";
import { AppKey } from "../../../config/constants";
import classesScss from "./Users.module.scss";

interface User {
  name: string;
  email: string;
  password: string;
  confirm_password: string;
  image: any;
  admin: boolean;
  about: string;
}

const AddUser: FC = () => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState("");
  const [loading, setloading] = useState(false);
  const { t } = useTranslation();
  const { handleSubmit, handleChange, data, errors, clearForm, setData } =
    useForm<User>({
      validations: {
        name: {
          custom: {
            isValid: (value) => value.length > 3,
            message: t("errors.reg_name_error"),
          },
        },
        email: {
          custom: {
            isValid: (value) => validateEmail(value),
            message: t("errors.invalid_email"),
          },
        },
        password: {
          custom: {
            isValid: (value) => value.length > 5,
            message: t("errors.invalid_password"),
          },
        },
        confirm_password: {
          custom: {
            isValid: (value): boolean => value === data.password,
            message: t("errors.invalid_confirm_password"),
          },
        },
      },
      onSubmit: () => AddBook(),
    });

  const handleCheckboxChange = (event: any) => {
    setData({ ...data, admin: event.target.checked });
  };

  const fileToDataUri = (file: any) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = (event) => {
        resolve(event!.target!.result);
      };
      reader.readAsDataURL(file);
    });

  const onFileChange = (file: any) => {
    if (!file) {
      setData({ ...data, image: "" });
      return;
    }

    fileToDataUri(file).then((dataUri) => {
      setData({ ...data, image: dataUri });
    });
  };

  const AddBook = async () => {
    setloading(true);

    await axios
      .post(
        "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=" +
          AppKey,
        { email: data.email, password: data.password, returnSecureToken: true }
      )
      .then((res: any) => {
        axios
          .put(
            `https://bookstore-fa5e2-default-rtdb.firebaseio.com/users/${res.data.localId}.json`,
            {
              email: data.email,
              name: data.name,
              uid: res.data.localId,
              token: res.data.idToken,
              refreshToken: res.data.refreshToken,
              image: data.image,
              about: data.about,
              admin: data.admin,
              password: data.password,
              confirm_password: data.confirm_password,
            }
          )
          .then((res: any) => {
            setResponse(res.data);
            clearForm();
            setError("");
          })
          .catch((err: any) => {
            setError(err.message);
          })
          .finally(() => {
            setloading(false);
          });
      })
      .catch((err: any) => {
        setError(err.message);
      })
      .finally(() => {
        setloading(false);
      });
  };
  let errorMessage = [];
  if (errors.name) {
    let el = (
      <>
        <Alert severity="error">{errors.name}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.email) {
    let el = (
      <>
        <Alert severity="error">{errors.email}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.password) {
    let el = (
      <>
        <Alert severity="error">{errors.password}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.confirm_password) {
    let el = (
      <>
        <Alert severity="error">{errors.confirm_password}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  if (error) {
    let el = (
      <>
        <Alert severity="error">{error}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  let load = null;
  if (loading) {
    load = (
      <>
        {" "}
        <CircularProgress />
        <br />
      </>
    );
  }

  let success = null;
  if (response) {
    success = (
      <>
        <Alert severity="success">{t("user_added")}</Alert>
        <br />
      </>
    );
  }

  let preview_image = null;
  if (data.image)
    preview_image = (
      <img
        width="200"
        className={classesScss.Image}
        height="200"
        src={data.image}
        alt="avatar"
      />
    );
  return (
    <>
      <p>{t("add_user")}</p>
      {load}
      {success}
      {errorMessage.map((el) => {
        return el;
      })}
      <form action="/" method="post" onSubmit={handleSubmit}>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <TextField
              name="name"
              label={t("name")}
              variant="outlined"
              onChange={handleChange("name")}
              required
              value={data.name || ""}
              fullWidth
            />
            <br />
            <br />
            <TextField
              name="email"
              label={t("email")}
              variant="outlined"
              onChange={handleChange("email")}
              required
              value={data.email || ""}
              fullWidth
            />
            <p>{t("image")}:</p>
            {preview_image}
            <TextField
              type="file"
              name="image"
              variant="outlined"
              fullWidth
              onChange={(event: any) =>
                onFileChange(event.target.files[0] || null)
              }
            />
            <br />
          </Grid>
          <Grid item xs={6}>
            <TextField
              name="password"
              label={t("password")}
              variant="outlined"
              onChange={handleChange("password")}
              required
              value={data.password || ""}
              fullWidth
            />
            <br />
            <br />

            <TextField
              name="confirm_password"
              label={t("confirm_password")}
              variant="outlined"
              onChange={handleChange("confirm_password")}
              required
              value={data.confirm_password || ""}
              fullWidth
            />
            <br />
            <br />

            <FormControlLabel
              control={
                <Checkbox
                  checked={data.admin || false}
                  onChange={handleCheckboxChange}
                  name="admin"
                  color="primary"
                />
              }
              label={t("is-admin")}
            />
            <br />
            <br />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="about"
              label={t("about")}
              variant="outlined"
              onChange={handleChange("about")}
              value={data.about || ""}
              rows={5}
              multiline
              fullWidth
            />
            <br />
            <br />
          </Grid>
        </Grid>

        <Button type="submit" variant="contained" color="primary">
          {t("send")}
        </Button>
      </form>
    </>
  );
};
export default withErrorBoundary(AddUser);
