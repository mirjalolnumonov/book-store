import React, { useEffect, useState } from "react";
import { Link, Redirect, withRouter } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Title from "../Title";
import * as Constants from "../../../config/routes/routes";
import { useTranslation } from "react-i18next";
import axios from "axios";
import { CircularProgress, DialogContentText } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { connect } from "react-redux";
import * as actionTypes from "../../../store/actions/index";
import withErrorBoundary from "../../../hocs/withErrorBoundary";
import { AppKey } from "../../../config/constants";
import classesScss from "./Users.module.scss";

function preventDefault(event: any) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

interface Response {
  identifier?: string;
  [key: string]: any;
}

let resFromFB: Response = {};

const ViewBooks = (props: any) => {
  const [response, setResponse] = useState(resFromFB);
  const [error, setError] = useState("");
  const [loading, setloading] = useState(false);
  const classes = useStyles();
  const { t } = useTranslation();
  const [del, setDel] = useState("");
  const [refreshToken, setRefreshToken] = useState("");
  const [open, setOpen] = React.useState(false);
  useEffect(() => {
    setloading(true);

    axios
      .get("https://bookstore-fa5e2-default-rtdb.firebaseio.com/users.json")
      .then((res: any) => {
        setResponse(res.data);
      })
      .catch((err: any) => {
        setError(err.message);
      })
      .finally(() => {
        setloading(false);
      });
  }, [del]);

  const handleEdit = (id: string) => {
     props.OnEditUser(response[id]);
     props.history.push(Constants.D_EDIT_USER);
  };

  const handleDelete = async () => {
    axios.post(
      "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=" +
        AppKey,
      { email: response[del].email, password: response[del].password, returnSecureToken: true }
    )
    .then((res) => {
      axios
      .post(
        `${"https://identitytoolkit.googleapis.com/v1/accounts:delete?key="}${AppKey}`,
        {"idToken": res.data.id_token }
      )
      .then(() => {
        axios
        .delete(
          `${"https://bookstore-fa5e2-default-rtdb.firebaseio.com/users"}/${del}.json`
        )
        .then(() => {
          setDel("true");
          setOpen(false);
        });
      });
    });   
  };

  const handleClickOpen = (id: string, refreshToken:string) => {
    setDel(id);
    setRefreshToken(refreshToken);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  let content = null;
  if (loading) {
    content = (
      <>
        <CircularProgress />
        <br />
      </>
    );
  }

  let resultArray: any = [];
  let el: Response = {};
  for (let res in response) {
    el = response[res];
    el.identifier = res;
    resultArray.push(el);
  }

  if (resultArray.length > 0) {
    content = (
      <>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>№</TableCell>
              <TableCell>{t("image")}</TableCell>
              <TableCell>{t("name")}</TableCell>
              <TableCell>{t("email")}</TableCell>
              <TableCell>{t("edit")}</TableCell>
              <TableCell>{t("delete")}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {resultArray.map((element: any, index: number) => (
              <TableRow>
                <TableCell>{index + 1}</TableCell>
                <TableCell><div className={classesScss.ImageCircle} style={{backgroundImage: "url("+element.image+")" }}></div></TableCell>
                <TableCell>{element.name}</TableCell>
                <TableCell>{element.email}</TableCell>
                <TableCell>
                  <Link to="#" onClick={() => handleEdit(element.identifier)}>
                    {" "}
                    {t("edit")}
                  </Link>
                </TableCell>
                <TableCell>
                  <Link
                    to="#"
                    onClick={() => handleClickOpen(element.identifier, element.refreshToken)}
                  >
                    {t("delete")}
                  </Link>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <br />
      </>
    );
  }
  if (!resultArray.length && !loading) {
    content = <p>{t("no_elements_yet")}</p>;
  }

  return (
    <React.Fragment>
      <Title>{t("users")}</Title>
      {content}

      <div className={classes.seeMore}>
        <Link to={Constants.D_ADD_USER}>{t("add_user")}</Link>
      </div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {t("delete_confirmation")}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            {t("cancel")}
          </Button>
          <Button onClick={handleDelete} color="primary" autoFocus>
            {t("delete")}
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    OnEditUser: (user: any) => dispatch(actionTypes.edituser(user)),
  };
};

export default connect(
  null,
  mapDispatchToProps
)(withErrorBoundary(withRouter(ViewBooks)));
