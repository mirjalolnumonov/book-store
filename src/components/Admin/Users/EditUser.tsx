import { FC, useState } from "react";
import { useForm } from "../../../customHooks/useForm";
import { useTranslation } from "react-i18next";
import { Alert } from "@material-ui/lab";
import axios, { AxiosError } from "axios";
import withErrorBoundary from "../../../hocs/withErrorBoundary";
import { Redirect, withRouter } from "react-router-dom";
import * as Routes from "../../../config/routes/routes";
import Checkbox from "@material-ui/core/Checkbox";
import { connect, useSelector } from "react-redux";
import { validateEmail } from "../../../utils/utils";
import {
  Button,
  CircularProgress,
  FormControlLabel,
  Grid,
  TextField,
} from "@material-ui/core";
import { AppKey } from "../../../config/constants";
import classesScss from "./Users.module.scss";

interface User {
  name: string;
  email: string;
  password: string;
  confirm_password: string;
  image: any;
  admin: boolean;
  about: string;
  token: string;
  refreshToken: string;
}
const EditUser: FC = (props: any) => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState("");
  const [loading, setloading] = useState(false);
  const { t } = useTranslation();
  const user = useSelector((state: any) => state.user.user);

  const { handleSubmit, handleChange, data, errors, clearForm, setData } =
    useForm<User>({
      validations: {
        name: {
          custom: {
            isValid: (value) => value.length > 3,
            message: t("errors.reg_name_error"),
          },
        },
        email: {
          custom: {
            isValid: (value) => validateEmail(value),
            message: t("errors.invalid_email"),
          },
        },
        password: {
          custom: {
            isValid: (value) => value.length > 5,
            message: t("errors.invalid_password"),
          },
        },
        confirm_password: {
          custom: {
            isValid: (value): boolean => value === data.password,
            message: t("errors.invalid_confirm_password"),
          },
        },
      },
      onSubmit: () => editBook(),
      initialValues: user,
    });

  const handleCheckboxChange = (event: any) => {
    setData({ ...data, admin: event.target.checked });
  };

  const fileToDataUri = (file: any) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = (event) => {
        resolve(event!.target!.result);
      };
      reader.readAsDataURL(file);
    });

  const onFileChange = (file: any) => {
    if (!file) {
      setData({ ...data, image: "" });
      return;
    }

    fileToDataUri(file).then((dataUri) => {
      setData({ ...data, image: dataUri });
    });
  };

  const editBook = async () => {
    setloading(true);
    console.log(user);
    axios
      .post(
        "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=" +
          AppKey,
        { email: user.email, password: user.password, returnSecureToken: true }
      )
      .then((resUp) => {
        axios
          .post(
            "https://identitytoolkit.googleapis.com/v1/accounts:update?key=" +
              AppKey,
            {
              idToken: resUp.data.idToken,
              password: data.password,
              returnSecureToken: true,
            }
          )
          .then((res) => {
            axios
              .patch(
                `https://bookstore-fa5e2-default-rtdb.firebaseio.com/users/${user.identifier}.json`,
                {
                  ...data,
                  token: res.data.idToken,
                  refreshToken: res.data.refreshToken,
                  uid: res.data.localId,
                }
              )
              .then((res: any) => {
                setResponse(res.data);
                setTimeout(() => {
                  props.history.push(Routes.D_USERS);
                }, 2000);
              })
              .catch((err: AxiosError): void => {
                if (err.response) {
                  setError(err.response.data.error.message);
                } else if (err.request) {
                  setError(err.request.data.toSting);
                } else {
                  setError("An internal error occurred");
                }
              })
              .finally(() => {
                setloading(false);
              });
          })
          .catch((err: AxiosError): void => {
            console.log(err.request);
            if (err.response) {
              setError(err.response.data.error.message);
            } else if (err.request) {
              setError(err.request.data.toSting);
            } else {
              setError("An internal error occurred");
            }
          })
          .finally(() => {
            setloading(false);
          });
      })
      .catch((err: AxiosError): void => {
        console.log(err);
        if (err.response) {
          setError(err.response.toString);
        } else if (err.request) {
          setError(err.request.data.toSting);
        } else {
          setError("An internal error occurred");
        }
      })
      .finally(() => {
        setloading(false);
      });
  };
  let errorMessage = [];
  if (errors.name) {
    let el = (
      <>
        <Alert severity="error">{errors.name}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.email) {
    let el = (
      <>
        <Alert severity="error">{errors.email}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.password) {
    let el = (
      <>
        <Alert severity="error">{errors.password}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.confirm_password) {
    let el = (
      <>
        <Alert severity="error">{errors.confirm_password}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  if (error) {
    let el = (
      <>
        <Alert severity="error">{error}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  let load = null;
  if (loading) {
    load = (
      <>
        {" "}
        <CircularProgress />
        <br />
      </>
    );
  }

  let success = null;
  if (response) {
    success = (
      <>
        <Alert severity="success">{t("user_edited")}</Alert>
        <br />
      </>
    );
  }

  if (!user.identifier) {
    props.history.push(Routes.D_USERS);
  }

  let preview_image = null;
  if(data.image) preview_image = <img width="200" className={classesScss.Image} height="200" src={data.image} alt="avatar"/>

  return (
    <>
      <p>{t("edit_book")}</p>
      {load}
      {success}
      {errorMessage.map((el) => {
        return el;
      })}
      <form action="/" method="post" onSubmit={handleSubmit}>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <TextField
              name="name"
              label={t("name")}
              variant="outlined"
              onChange={handleChange("name")}
              required
              value={data.name || ""}
              fullWidth
            />
            <br />
            <br />
            <TextField
              name="email"
              label={t("email")}
              variant="outlined"
              onChange={handleChange("email")}
              required
              value={data.email || ""}
              fullWidth
              disabled
            />
            <p>{t("image")}:</p>
            {preview_image}
            <TextField
              type="file"
              name="image"
              variant="outlined"
              fullWidth
              onChange={(event:any) => onFileChange(event.target.files[0] || null)}
            />
            <br />
            <br />
          </Grid>
          <Grid item xs={6}>
            <TextField
              name="password"
              label={t("password")}
              variant="outlined"
              onChange={handleChange("password")}
              required
              value={data.password || ""}
              fullWidth
            />
            <br />
            <br />

            <TextField
              name="confirm_password"
              label={t("confirm_password")}
              variant="outlined"
              onChange={handleChange("confirm_password")}
              required
              value={data.confirm_password || ""}
              fullWidth
            />
            <br />
            <br />

            <FormControlLabel
              control={
                <Checkbox
                  checked={data.admin || false}
                  onChange={handleCheckboxChange}
                  name="admin"
                  color="primary"
                />
              }
              label={t("is-admin")}
            />
            <br />
            <br />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="about"
              label={t("about")}
              variant="outlined"
              onChange={handleChange("about")}
              value={data.about || ""}
              rows={5}
              multiline
              fullWidth
            />
            <br />
            <br />
          </Grid>
        </Grid>

        <Button type="submit" variant="contained" color="primary">
          {t("send")}
        </Button>
      </form>
    </>
  );
};

const mapStateToProps = (state: any) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps)(
  withErrorBoundary(withRouter(EditUser))
);
