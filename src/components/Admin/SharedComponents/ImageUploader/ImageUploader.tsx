import React from "react";
import { useState } from "react";
import {DropzoneArea} from 'material-ui-dropzone';


const FileUploadComponent = () => {
  const [files, setFiles] = useState({files:[]});
 const handleChange = (files: any) => {
  setFiles({
    files: files
  });
}
  return (
    <div>
      <h3>Change profile image</h3>
      <DropzoneArea
        onChange={handleChange}
        />
    </div>
  );
};

export default FileUploadComponent;
