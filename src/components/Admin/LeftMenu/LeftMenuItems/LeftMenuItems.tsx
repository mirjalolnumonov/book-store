import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import { Link, useRouteMatch } from "react-router-dom";
import {
  AccountBox,
  Category,
  Facebook,
  LibraryBooks,
  Lock,
  Person,
} from "@material-ui/icons";
import People from "@material-ui/icons/People";
import { Divider } from "@material-ui/core";
import { useTranslation } from "react-i18next";

export const MainListItems = () => {
  const { t } = useTranslation();
  let { path, url } = useRouteMatch();
  return (
    <div>
      <Link to={`${url}/profile`}>
        <ListItem button>
          <ListItemIcon>
            <Person />
          </ListItemIcon>
          <ListItemText primary={t("user")} />
        </ListItem>
      </Link>
      <Link to={`${url}/users`}>
        <ListItem button>
          <ListItemIcon>
            <PeopleAltIcon />
          </ListItemIcon>
          <ListItemText primary={t("users")} />
        </ListItem>
      </Link>
      <Link to={`${url}/categories`}>
        <ListItem button>
          <ListItemIcon>
            <Category />
          </ListItemIcon>
          <ListItemText primary={t("categories")} />
        </ListItem>
      </Link>
      <Link to={`${url}/books`}>
        <ListItem button>
          <ListItemIcon>
            <LibraryBooks />
          </ListItemIcon>
          <ListItemText primary={t("books")} />
        </ListItem>
      </Link>
      <Link to={`${url}/orders`}>
        <ListItem button>
          <ListItemIcon>
            <ShoppingCartIcon />
          </ListItemIcon>
          <ListItemText primary={t("orders")} />
        </ListItem>
      </Link>
      <Link to={`${url}/socials`}>
        <ListItem button>
          <ListItemIcon>
            <Facebook />
          </ListItemIcon>
          <ListItemText primary={t("social_networks")}/>
        </ListItem>
      </Link>
      <Divider />
      <ListSubheader inset>{t("settings")}</ListSubheader>
    <Link to={`${url}/editprofile`}>
      <ListItem button>
        <ListItemIcon>
          <AccountBox />
        </ListItemIcon>
        <ListItemText primary={t("edit_profile")}/>
      </ListItem>
    </Link>
    </div>
  );
};
