import { FC, useState } from "react";
import { useForm } from "../../../customHooks/useForm";
import { useTranslation } from "react-i18next";
import { Alert } from "@material-ui/lab";
import axios from "axios";
import withErrorBoundary from "../../../hocs/withErrorBoundary";
import { Redirect, withRouter } from "react-router-dom";
import * as Routes from "../../../config/routes/routes";
import Checkbox from "@material-ui/core/Checkbox";
import { connect, useSelector } from "react-redux";
import {
  Button,
  CircularProgress,
  FormControlLabel,
  Grid,
  TextField,
} from "@material-ui/core";
import classesScss from "./Books.module.scss";

interface Book {
  name_ru: string;
  name_en: string;
  preview_text_ru: string;
  preview_text_en: string;
  price: number;
  year: number;
  text_en: string;
  text_ru: string;
  image: any;
  slider: boolean;
}
const EditBook: FC = (props: any) => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState("");
  const [loading, setloading] = useState(false);
  const { t } = useTranslation();
  const book = useSelector((state: any) => state.buk.book);

  const { handleSubmit, handleChange, data, errors, clearForm, setData } =
    useForm<Book>({
      validations: {
        name_ru: {
          required: {
            value: true,
            message: t("errors.required_error"),
          },
        },
        name_en: {
          required: {
            value: true,
            message: t("errors.required_error"),
          },
        },
        preview_text_ru: {
          required: {
            value: true,
            message: t("errors.required_error"),
          },
        },
        preview_text_en: {
          required: {
            value: true,
            message: t("errors.required_error"),
          },
        },
        price: {
          custom: {
            isValid: (value) => parseInt(value) > 0,
            message: t("errors.price_error"),
          },
        },
        year: {
          custom: {
            isValid: (value) => parseInt(value) > 1700,
            message: t("errors.year_error"),
          },
        },
      },
      onSubmit: () => editBook(),
      initialValues: book,
    });

  const handleCheckboxChange = (event: any) => {
    setData({ ...data, slider: event.target.checked });
  };

  const fileToDataUri = (file: any) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = (event) => {
        resolve(event!.target!.result);
      };
      reader.readAsDataURL(file);
    });

  const onFileChange = (file: any) => {
    if (!file) {
      setData({ ...data, image: "" });
      return;
    }

    fileToDataUri(file).then((dataUri) => {
      setData({ ...data, image: dataUri });
    });
  };

  const editBook = async () => {
    setloading(true);
    await axios
      .put(
        `https://bookstore-fa5e2-default-rtdb.firebaseio.com/books/${book.identifier}.json`,
        data
      )
      .then((res: any) => {
        setResponse(res.data);
        setTimeout(() => {
          props.history.push(Routes.D_BOOKS);
        }, 2000);
      })
      .catch((err: any) => {
        setError(err.message);
      })
      .finally(() => {
        setloading(false);
      });
  };
  let errorMessage = [];
  if (
    errors.name_ru ||
    errors.name_en ||
    errors.preview_text_ru ||
    errors.preview_text_en
  ) {
    let el = (
      <>
        <Alert severity="error">{errors.name_ru}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.price) {
    let el = (
      <>
        <Alert severity="error">{errors.price}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.year) {
    let el = (
      <>
        <Alert severity="error">{errors.year}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  if (error) {
    let el = (
      <>
        <Alert severity="error">{error}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  let load = null;
  if (loading) {
    load = (
      <>
        {" "}
        <CircularProgress />
        <br />
      </>
    );
  }

  let success = null;
  if (response) {
    success = (
      <>
        <Alert severity="success">{t("book_added")}</Alert>
        <br />
      </>
    );
  }

  if (!book.identifier) {
    props.history.push(Routes.D_BOOKS);
  }

  let preview_image = null;
  if(data.image) preview_image = <img width="200" className={classesScss.Image} height="200" src={data.image} alt="avatar"/>

  return (
    <>
      <p>{t("edit_book")}</p>
      {load}
      {success}
      {errorMessage.map((el) => {
        return el;
      })}
      <form action="/" method="post" onSubmit={handleSubmit}>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <TextField
              name="name_en"
              label={t("name_en")}
              variant="outlined"
              onChange={handleChange("name_en")}
              required
              value={data.name_en || ""}
              fullWidth
            />
            <br />
            <br />
            <TextField
              name="name_ru"
              label={t("name_ru")}
              variant="outlined"
              onChange={handleChange("name_ru")}
              required
              value={data.name_ru || ""}
              fullWidth
            />
            <br />
            <br />
            <TextField
              name="price"
              label={t("price")}
              variant="outlined"
              onChange={handleChange("price")}
              required
              value={data.price || ""}
              fullWidth
            />
            <br />
            <br />
          </Grid>
          <Grid item xs={6}>
            <TextField
              name="year"
              label={t("year")}
              variant="outlined"
              onChange={handleChange("year")}
              required
              value={data.year || ""}
              fullWidth
            />
            <br />
            <br />
            <FormControlLabel
              control={
                <Checkbox
                  checked={data.slider || false}
                  onChange={handleCheckboxChange}
                  name="slider"
                  color="primary"
                />
              }
              label={t("book-slider")}
            />
           
            <p>{t("image")}:</p>
            <br />
            {preview_image}
            <br />
            <TextField
              type="file"
              name="image"
              variant="outlined"
              fullWidth
              onChange={(event:any) => onFileChange(event.target.files[0] || null)}
            />
            <br />
            <br />
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="preview_text_ru"
              label={t("preview_text_ru")}
              variant="outlined"
              onChange={handleChange("preview_text_ru")}
              required
              value={data.preview_text_ru || ""}
              rows={5}
              multiline
              fullWidth
            />
            <br />
            <br />
            <TextField
              name="preview_text_en"
              label={t("preview_text_en")}
              variant="outlined"
              onChange={handleChange("preview_text_en")}
              required
              value={data.preview_text_en || ""}
              rows={5}
              multiline
              fullWidth
            />
            <br />
            <br />
            <TextField
              name="text_ru"
              label={t("text_ru")}
              variant="outlined"
              rows={5}
              multiline
              fullWidth
              onChange={handleChange("text_ru")}
              value={data.text_ru || ""}
            />
            <br />
            <br />
            <TextField
              name="text_en"
              label={t("text_en")}
              variant="outlined"
              rows={5}
              multiline
              fullWidth
              onChange={handleChange("text_en")}
              value={data.text_en || ""}
            />
            <br />
            <br />
          </Grid>
        </Grid>

        <Button type="submit" variant="contained" color="primary">
          {t("send")}
        </Button>
      </form>
    </>
  );
};

const mapStateToProps = (state: any) => {
  return {
    book: state.book,
  };
};
export default connect(mapStateToProps)(
  withErrorBoundary(withRouter(EditBook))
);
