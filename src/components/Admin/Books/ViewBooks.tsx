import React, { useEffect, useState } from "react";
import { Link, Redirect, withRouter } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Title from "../Title";
import * as Constants from "../../../config/routes/routes";
import { useTranslation } from "react-i18next";
import axios from "axios";
import { CircularProgress, DialogContentText } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { connect } from "react-redux";
import * as actionTypes from "../../../store/actions/index";
import withErrorBoundary from "../../../hocs/withErrorBoundary";
import classesScss from "./Books.module.scss";

function preventDefault(event: any) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

interface Response {
  identifier?: string;
  [key: string]: any;
}

let resFromFB: Response = {};

const ViewBooks = (props: any) => {
  const [response, setResponse] = useState(resFromFB);
  const [error, setError] = useState("");
  const [loading, setloading] = useState(false);
  const classes = useStyles();
  const { t } = useTranslation();
  const [del, setDel] = useState("");
  const [open, setOpen] = React.useState(false);
  useEffect(() => {
    setloading(true);

    axios
      .get("https://bookstore-fa5e2-default-rtdb.firebaseio.com/books.json")
      .then((res: any) => {
        setResponse(res.data);
      })
      .catch((err: any) => {
        setError(err.message);
      })
      .finally(() => {
        setloading(false);
      });
  }, [del]);

  const handleEdit = (id: string) => {
    props.OnEditBook(response[id]);
    props.history.push(Constants.D_EDIT_BOOK);
  };

  const handleDelete = () => {
    axios
      .delete(
        `${"https://bookstore-fa5e2-default-rtdb.firebaseio.com/books"}/${del}.json`
      )
      .then(() => {
        setDel("true");
        setOpen(false);
      });
  };

  const handleClickOpen = (id: string) => {
    setDel(id);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  let content = null;
  if (loading) {
    content = (
      <>
        <CircularProgress />
        <br />
      </>
    );
  }

  let resultArray: any = [];
  let el: Response = {};
  for (let res in response) {
    el = response[res];
    el.identifier = res;
    resultArray.push(el);
  }

  if (resultArray.length > 0) {
    content = (
      <>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>№</TableCell>
              <TableCell>{t("name_en")}</TableCell>
              <TableCell>{t("name_ru")}</TableCell>
              <TableCell>{t("image")}</TableCell>
              <TableCell>{t("price")}($)</TableCell>
              <TableCell>{t("year")}</TableCell>
              <TableCell>{t("preview_text_en")}</TableCell>
              <TableCell>{t("preview_text_ru")}</TableCell>
              <TableCell>{t("edit")}</TableCell>
              <TableCell>{t("delete")}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {resultArray.map((element: any, index: number) => (
              <TableRow>
                <TableCell>{index + 1}</TableCell>
                <TableCell>{element.name_en}</TableCell>
                <TableCell>{element.name_ru}</TableCell>
                <TableCell><img className={classesScss.Image} src={element.image} alt={element.name_en}/></TableCell>
                <TableCell>{element.price}</TableCell>
                <TableCell>{element.year}</TableCell>
                <TableCell>{element.preview_text_en}</TableCell>
                <TableCell>{element.preview_text_ru}</TableCell>
                <TableCell>
                  <Link to="#" onClick={() => handleEdit(element.identifier)}>
                    {" "}
                    {t("edit")}
                  </Link>
                </TableCell>
                <TableCell>
                  <Link
                    to="#"
                    onClick={() => handleClickOpen(element.identifier)}
                  >
                    {t("delete")}
                  </Link>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <br />
      </>
    );
  }
  if (!resultArray.length && !loading) {
    content = <p>{t("no_elements_yet")}</p>;
  }

  return (
    <React.Fragment>
      <Title>{t("books")}</Title>
      {content}

      <div className={classes.seeMore}>
        <Link to={Constants.D_ADD_BOOK}>{t("add_book")}</Link>
      </div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {t("delete_confirmation")}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            {t("cancel")}
          </Button>
          <Button onClick={handleDelete} color="primary" autoFocus>
            {t("delete")}
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    OnEditBook: (book: any) => dispatch(actionTypes.editbook(book)),
  };
};

export default connect(
  null,
  mapDispatchToProps
)(withErrorBoundary(withRouter(ViewBooks)));
