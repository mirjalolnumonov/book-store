import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import { Button, Grid } from "@material-ui/core";
import book_1 from "../../../../assets/images/books/book_1.jpg";
import book_2 from "../../../../assets/images/books/book_2.png";
import book_3 from "../../../../assets/images/books/book_3.jpg";
import classes from "./TopSlider.module.scss";

const TopSlider: React.FC = () => {
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <Slider {...settings}>
      <div>
        <Grid container>
          <Grid item xs={3}>
            <img
              className={classes.Image}
              src={book_1}
              alt=""
            />
          </Grid>
          <Grid item xs={9}>
            <h2>Eloquent Javascript</h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque
              aliquam doloribus, nulla, voluptatem, velit ullam reiciendis rerum
              consequatur ratione perspiciatis assumenda sunt vel animi facilis
              autem nihil adipisci cumque vitae. Lorem, ipsum dolor sit amet
              consectetur adipisicing elit. Quas, quasi laboriosam repellendus
              tempore ex labore sapiente omnis aliquam debitis voluptas
              perspiciatis deserunt porro earum fuga, voluptatem hic quis,
              blanditiis accusantium!<br/>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque
              aliquam doloribus, nulla, voluptatem, velit ullam reiciendis rerum
              consequatur ratione perspiciatis assumenda sunt vel animi facilis
              autem nihil adipisci cumque vitae. Lorem, ipsum dolor sit amet
              consectetur adipisicing elit. Quas, quasi laboriosam repellendus
              tempore ex labore sapiente omnis aliquam debitis voluptas
              perspiciatis deserunt porro earum fuga, voluptatem hic quis,
              blanditiis accusantium!
            </p>
            <Button variant="outlined" color="primary">More</Button>
          </Grid>
        </Grid>
      </div>
      <div>
        <Grid container>
          <Grid item xs={3}>
            <img className={classes.Image} src={book_2} alt="" />
          </Grid>
          <Grid item xs={9}>
            <h2>Js 6 volumes set</h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque
              aliquam doloribus, nulla, voluptatem, velit ullam reiciendis rerum
              consequatur ratione perspiciatis assumenda sunt vel animi facilis
              autem nihil adipisci cumque vitae. Lorem ipsum dolor sit amet
              consectetur, adipisicing elit. Optio excepturi illo, tempora
              praesentium facilis fuga consequuntur dolorem modi, nulla
              repudiandae voluptatum esse, sapiente autem. Magni totam
              voluptatem praesentium doloribus itaque?<br/>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque
              aliquam doloribus, nulla, voluptatem, velit ullam reiciendis rerum
              consequatur ratione perspiciatis assumenda sunt vel animi facilis
              autem nihil adipisci cumque vitae. Lorem, ipsum dolor sit amet
              consectetur adipisicing elit. Quas, quasi laboriosam repellendus
              tempore ex labore sapiente omnis aliquam debitis voluptas
              perspiciatis deserunt porro earum fuga, voluptatem hic quis,
              blanditiis accusantium!
            </p>
            <Button variant="outlined" color="primary">More</Button>
          </Grid>
        </Grid>
      </div>
      <div>
        <Grid container>
          <Grid item xs={3}>
            <img className={classes.Image} src={book_3} alt="" />
          </Grid>
          <Grid item xs={9}>
            <h2>Browser Networking</h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque
              aliquam doloribus, nulla, voluptatem, velit ullam reiciendis rerum
              consequatur ratione perspiciatis assumenda sunt vel animi facilis
              autem nihil adipisci cumque vitae. Lorem ipsum dolor sit amet
              consectetur adipisicing elit. Iure maiores iusto illo corrupti,
              unde consectetur cum. Reiciendis, ad placeat reprehenderit
              corporis tenetur ea quaerat! Voluptates aspernatur tempora et
              quasi atque.<br/>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque
              aliquam doloribus, nulla, voluptatem, velit ullam reiciendis rerum
              consequatur ratione perspiciatis assumenda sunt vel animi facilis
              autem nihil adipisci cumque vitae. Lorem, ipsum dolor sit amet
              consectetur adipisicing elit. Quas, quasi laboriosam repellendus
              tempore ex labore sapiente omnis aliquam debitis voluptas
              perspiciatis deserunt porro earum fuga, voluptatem hic quis,
              blanditiis accusantium!
            </p>
            <Button variant="outlined" color="primary">More</Button>
          </Grid>
        </Grid>
      </div>
    </Slider>
  );
};

export default TopSlider;
