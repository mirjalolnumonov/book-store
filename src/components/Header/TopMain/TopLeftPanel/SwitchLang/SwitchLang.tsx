import React from "react";
import Button from "@material-ui/core/Button";
import { display } from "@material-ui/system";
import i18n from "../../../../../i18n";
import { SyntheticEvent } from "react";
const SwitchLang: React.FC = (props:any) => {

  const changeLanguage = (e:SyntheticEvent, lng:string) => {
    i18n.changeLanguage(lng);
    localStorage.setItem("lang", lng);
    e.preventDefault();
  }
  return (
    <div style={{ display: "flex", justifyContent: 'space-evenly', marginBottom: '20px' }}>
      <Button onClick={(event: SyntheticEvent)=>changeLanguage(event, "en")}  variant="contained" color="primary">
        En
      </Button>
      <Button onClick={(event:SyntheticEvent)=>changeLanguage(event, "ru")}  variant="contained" color="primary">
        Ru
      </Button>
    </div>
  );
};

export default SwitchLang;
