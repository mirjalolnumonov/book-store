import React, { useState } from "react";
import classes from "./AuthReg.module.scss";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormGroup from "@material-ui/core/FormGroup";
import { makeStyles } from "@material-ui/core/styles";
import { useTranslation } from "react-i18next";
import { useForm } from "../../../../../customHooks/useForm";
import { CircularProgress } from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import axios from "axios";
import { validateEmail } from "../../../../../utils/utils";
import { AppKey } from "../../../../../config/constants";

const useStyles = makeStyles({
  Button: {
    marginBottom: "20px",
    marginTop: "20px",
  },
  ButtonAR: {
    marginTop: "10px",
  },
});

interface Auth {
  email: string;
  password: string;
}

const Authorization = (props: any) => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState("");
  const [loading, setloading] = useState(false);
  const mUiclasses = useStyles();
  const { t } = useTranslation();

  const { handleSubmit, handleChange, data, errors, clearForm } = useForm<Auth>(
    {
      validations: {
        email: {
          custom: {
            isValid: (value) => validateEmail(value),
            message: t("errors.invalid_email"),
          },
        },
        password: {
          custom: {
            isValid: (value) => value.length > 3,
            message: t("errors.invalid_password"),
          },
        },
      },
      onSubmit: () => Authorize(),
    }
  );

  const Authorize = async () => {
    setloading(true);

    await axios
      .post(
        "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=" +
          AppKey,
        { email: data.email, password: data.password, returnSecureToken: true }
      )
      .then((res: any) => {
        axios
          .patch(
            `https://bookstore-fa5e2-default-rtdb.firebaseio.com/users/${res.data.localId}.json`,
            {
              uid: res.data.localId,
              token: res.data.idToken,
              refreshToken: res.data.refreshToken,
            }
          )
          .then((response) => {
            if (res.data.registered) {
              setResponse(res.data.registered);
              setError("");
              sessionStorage.setItem("auth", res.data.localId);
              props.handleAuthorization(res.data.localId);
              props.closeHandle();
            } else {
              setError(t("invalid_email_or_password"));
            }
            // clearForm();
          })
          .catch((err: any) => {
            setError(err.message);
          })
          .finally(() => {
            setloading(false);
          });
      })
      .catch((err) => {
        setError(err.message);
      })
      .finally(() => {
        setloading(false);
      });
  };
  let errorMessage = [];
  if (errors.email) {
    let el = (
      <>
        <Alert severity="error">{errors.email}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.password) {
    let el = (
      <>
        <Alert severity="error">{errors.password}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (error) {
    let el = (
      <>
        <Alert severity="error">{error}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  let load = null;
  if (loading) {
    load = (
      <>
        {" "}
        <CircularProgress />
        <br />
      </>
    );
  }

  let success = null;
  if (response && !error) {
    success = (
      <>
        <Alert severity="success">{t("authorization_success")}</Alert>
        <br />
      </>
    );
  }

  return (
    <FormGroup>
      <h2>{t("authorization")}</h2>
      {load}
      {success}
      {errorMessage.map((el) => {
        return el;
      })}
      <form noValidate autoComplete="off" onSubmit={handleSubmit}>
        <TextField
          label={t("email")}
          name="email"
          className={`${classes.Fullwith}`}
          onChange={handleChange("email")}
          required
          value={data.email || ""}
        />
        <br />
        <TextField
          label={t("password")}
          name="password"
          className={`${classes.Fullwith}`}
          onChange={handleChange("password")}
          required
          value={data.password || ""}
        />
        <br />
        <Button
          type="submit"
          variant="contained"
          color="primary"
          className={`${mUiclasses.Button} ${classes.Fullwith}`}
        >
          {t("submit")}
        </Button>
        <br />
      </form>
    </FormGroup>
  );
};

export default Authorization;
