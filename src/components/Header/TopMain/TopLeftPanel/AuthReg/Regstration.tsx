import React, { useState } from "react";
import classes from "./AuthReg.module.scss";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormGroup from "@material-ui/core/FormGroup";
import { makeStyles } from "@material-ui/core/styles";
import { useTranslation } from "react-i18next";
import { useForm } from "../../../../../customHooks/useForm";
import { CircularProgress } from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import axios from "axios";
import { validateEmail } from "../../../../../utils/utils";
import { AppKey } from "../../../../../config/constants"

const useStyles = makeStyles({
  Button: {
    marginBottom: "20px",
    marginTop: "20px",
  },
  ButtonAR: {
    marginTop: "10px",
  },
});

interface Reg {
  name: string;
  email: string;
  password: string;
  confirm_password: string;
}

const Registration = (props:any) => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState("");
  const [loading, setloading] = useState(false);
  const mUiclasses = useStyles();
  const { t } = useTranslation();

  const { handleSubmit, handleChange, data, errors, clearForm } = useForm<Reg>({
    validations: {
      name: {
        custom: {
          isValid: (value) => value.length > 3,
          message: t("errors.reg_name_error"),
        },
      },
      email: {
        custom: { 
          isValid: (value) => validateEmail(value),
          message: t("errors.invalid_email"),
        },
      },
      password: {
        custom: {
          isValid: (value) => value.length > 5,
          message: t("errors.invalid_password"),
        },
      },
      confirm_password: {
        custom: {
          isValid: (value):boolean => value === data.password,
          message: t("errors.invalid_confirm_password"),
        },
      },
    },
    onSubmit: () => Register(),
  });

  const Register = async () => {
    setloading(true);
    await axios
      .post(
        "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key="+AppKey,
        {...data}
      )
      .then((res: any) => {
        axios
        .put(
          `https://bookstore-fa5e2-default-rtdb.firebaseio.com/users/${res.data.localId}.json`,
          {"email": data.email, "name": data.name, "uid": res.data.localId, "token": res.data.idToken, "refreshToken": res.data.refreshToken}
        )
        .then((res: any) => {
          setResponse(res.data);
          clearForm();
          setError("");
          setTimeout(()=>{
            props.handleAuth(true);
          }, 1000);
        })
        .catch((err: any) => {
          setError(err.message);
        })
        .finally(() => {
          setloading(false);
        });
      })
      .catch((err: any) => {
        setError(err.message);
      })
      .finally(() => {
        setloading(false);
      });
  };
  let errorMessage = [];
  if (errors.name) {
    let el = (
      <>
        <Alert severity="error">{errors.name}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.email) {
    let el = (
      <>
        <Alert severity="error">{errors.email}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.password) {
    let el = (
      <>
        <Alert severity="error">{errors.password}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }
  if (errors.confirm_password) {
    let el = (
      <>
        <Alert severity="error">{errors.confirm_password}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  if (error) {
    let el = (
      <>
        <Alert severity="error">{error}!</Alert>
        <br />
      </>
    );
    errorMessage.push(el);
  }

  let load = null;
  if (loading) {
    load = (
      <>
        {" "}
        <CircularProgress />
        <br />
      </>
    );
  }

  let success = null;
  if (response && !error) {
    success = (
      <>
        <Alert severity="success">{t("registration_success")}</Alert>
        <br />
      </>
    );
  }

  return (
    <FormGroup>
      <h2>{t("registration")}</h2>
      {load}
      {success}
      {errorMessage.map((el) => {
        return el;
      })}
      <form action="" method="post" onSubmit={handleSubmit}>
        <TextField
          name="name"
          className={`${classes.Fullwith}`}
          label={t("name")}
          onChange={handleChange("name")}
          required
          value={data.name || ""}
        />
        <br />
        <TextField
          name="email"
          className={`${classes.Fullwith}`}
          label={t("email")}
          onChange={handleChange("email")}
          required
          value={data.email || ""}
        />
        <br />
        <TextField
          name="password"
          type="password"
          className={`${classes.Fullwith}`}
          label={t("password")}
          onChange={handleChange("password")}
          required
          value={data.password || ""}
        />
        <br />
        <TextField
          type="password"
          name="confirm_password"
          className={`${classes.Fullwith}`}
          label={t("confirm_password")}
          onChange={handleChange("confirm_password")}
          required
          value={data.confirm_password || ""}
        />
        <br />
        <Button type='submit'
          variant="contained"
          className={`${mUiclasses.Button} ${classes.Fullwith}`}
          color="primary"
        >
          {t("submit")}
        </Button>
        <br />
      </form>
    </FormGroup>
  );
};

export default Registration;
