import React, {useState} from "react";
import classes from "./AuthReg.module.scss";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from "react-i18next";
import Registration from "./Regstration";
import AuthAuthorization from "./Authorization";
import { DASHBOARD } from "../../../../../config/routes/routes";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  Button: {
    marginBottom: '20px',
    marginTop: '20px'
  },
  ButtonAR: {
    marginTop: '10px'
  },
});



const AuthReg: React.FC = () => {
  const [open, setOpen] = useState(false);
  const [auth, setAuth] = useState(true);
  const [authorization, setAuthorization] = useState("");
  
  const mUiclasses = useStyles();
  const { t } = useTranslation();
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleAuth = (type = false) => {
    if (type) {
      if (auth) return false;
      setAuth(!auth);
    } else {
      if (!auth) return;
      setAuth(false);
    }
  };

  const handleLogout = ()=>{
    sessionStorage.setItem("auth", "");
    setAuthorization("");
  }


  let form = <Registration handleAuth={handleAuth}/>;
  if (auth) {
    form = <AuthAuthorization handleAuthorization = {setAuthorization} closeHandle={handleClose}/>
  }

  let button = (
    <Button color="primary" className={mUiclasses.ButtonAR} onClick={handleClickOpen}>
        {t("authreg")}
      </Button>
  )

  let authorizationSessionStorage = (sessionStorage.getItem("auth"))?sessionStorage.getItem("auth"):null;

  if((authorization && authorization.length > 2) || (authorizationSessionStorage!=null && authorizationSessionStorage.length > 2)){
     button = (
      <>
      <Link to="#" onClick={handleLogout} color="primary" className={mUiclasses.ButtonAR}>
          {t("logout")}
      </Link> /  
      <Link to={DASHBOARD} color="primary" className={mUiclasses.ButtonAR}>
        {t("cabinet")}
      </Link>
      </>
    )
  }
  return (
    <div>
      {button}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <div>
          <ul className={classes.Tabs}>
            <li>
              <a href="#auth" onClick={() => handleAuth(true)}>
              {t("authorization")}
              </a>
            </li>
            <li>
              <a href="#reg" onClick={() => handleAuth()}>
              {t("registration")}
              </a>
            </li>
          </ul>
          <div className={classes.AuthReg}>
              {form}
          </div>
        </div>
      </Dialog>
    </div>
  );
};

export default AuthReg;
