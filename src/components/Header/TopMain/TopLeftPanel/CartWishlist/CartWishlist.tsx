import React from "react";
import Button from "@material-ui/core/Button";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import FavoriteIcon from "@material-ui/icons/Favorite";
const CartWishlist: React.FC = () => {
  return (
    <div style={{ display: "flex",  justifyContent: 'space-evenly', marginBottom: '20px' }}>
      <Button variant="outlined" color="primary">
        <FavoriteIcon />
      </Button>
      <Button variant="outlined" color="primary">
        <ShoppingCartIcon />
      </Button>
    </div>
  );
};

export default CartWishlist;
