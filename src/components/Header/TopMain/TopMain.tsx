import React from "react";
import Grid from "@material-ui/core/Grid";
import Logo from "./Logo/Logo";
import TopSlider from "./TopSlider/TopSlider";
import SecondMenu from "./SecondMenu/SecondMenu";
import AuthReg from "./TopLeftPanel/AuthReg/Authreg";
import classesS from "./TopMain.module.scss";
import { makeStyles } from "@material-ui/core/styles";
import SwitchLang from "./TopLeftPanel/SwitchLang/SwitchLang";
import CartWishlist from "./TopLeftPanel/CartWishlist/CartWishlist";
import Search from "./TopLeftPanel/Search/Serach";

const useStyles = makeStyles({
  Slider: {
    marginBottom: "40px",
  },
});
const TopMain: React.FC = () => {
  const classes = useStyles();
  return (
    <>
      <Grid container spacing={5}>
        <Grid item xs={9}>
          <Grid item xs={12} className={classes.Slider}>
            <TopSlider />
          </Grid>
          <Grid item xs={12}>
            <SecondMenu />
          </Grid>
        </Grid>
        <Grid item xs={3} className={classesS.LeftPanel}>
          <div className={classesS.innerDiv}>
          <AuthReg />
          <Logo />
          <SwitchLang />
          <CartWishlist />
          <div className={classesS.Search}>
            <Search />
          </div>
          </div>
        </Grid>
      </Grid>
    </>
  );
};

export default TopMain;
