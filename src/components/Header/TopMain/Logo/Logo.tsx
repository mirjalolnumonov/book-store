import React from 'react';
import { Link } from 'react-router-dom';
import Image  from "../../../../assets/images/logo.png";
import classes from "./Logo.module.scss"
const Logo: React.FC = () => {
    return (
        <div className={classes.imageWrapper}>
            <Link to="/"><img className={classes.image} src={Image} alt="Book store"/></Link>
        </div>
    )
}

export default Logo;