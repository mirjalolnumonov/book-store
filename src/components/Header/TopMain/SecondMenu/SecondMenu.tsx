import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import { Divider } from "@material-ui/core";
import classes from "./SecondMenu.module.scss";
import { Categories } from "../../../../models/interfaces/Categories";
import data from '../../../../assets/json/categories.json';

const categories: Categories[] = data;

const SecondMenu: React.FC = (): JSX.Element => {

  const elems = categories.map(el => {
    return (
      <Grid item xs key={el.id} className={classes.Item}>
          <Link href="#">{el.name}</Link>
      </Grid>
    ) 
  });
  return (
    <>
      <Grid container xs={12} className={classes.SecondMenu}>
        {elems}
      </Grid>
    </>
  );
};

export default SecondMenu;
