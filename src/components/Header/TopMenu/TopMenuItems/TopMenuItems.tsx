import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import { Divider } from "@material-ui/core";
import classes from "./TopMenu.module.scss";
import { TopMenuElems } from "../../../../models/interfaces/TopMenu";
import data from '../../../../assets/json/techs.json';
import MouseOverPopover from "../MouseOverPopover/MouseOverPopover";

const techs: TopMenuElems[] = data;

const TopMenu: React.FC = (): JSX.Element => {

  const elems = techs.map(el => {
    return (
      <Grid item xs key={el.id}>
        <MouseOverPopover element={el} />
      </Grid>
    ) 
  });
  return (
    <>
      <Grid container spacing={3} className={classes.topMenu}>
        {elems}
      </Grid>
      <Divider />
    </>
  );
};

export default TopMenu;
