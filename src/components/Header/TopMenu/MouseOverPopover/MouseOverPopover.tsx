import React from "react";
import Popover from "@material-ui/core/Popover";
import Typography from "@material-ui/core/Typography";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import List from "@material-ui/core/List";
import ListItem, { ListItemProps } from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { useTranslation } from "react-i18next";
import i18n from "../../../../i18n/index";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    popover: {
      pointerEvents: "none",
    },
    paper: {
      padding: theme.spacing(1),
    },
    root: {
      width: "100%",
      maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
    },
  })
);

function ListItemLink(props: ListItemProps<"a", { button?: true }>) {
  return <ListItem button component="a" {...props} />;
}

export default function MouseOverPopover(props: any) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);
  const { t } = useTranslation();

  const handlePopoverOpen = (
    event: React.MouseEvent<HTMLElement, MouseEvent>
  ) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  document.addEventListener("click", function () {
    handlePopoverClose();
  });
  const open = Boolean(anchorEl);

  return (
    <div>
      <Typography
        aria-owns={open ? "mouse-over-popover" : undefined}
        aria-haspopup="true"
        onMouseEnter={handlePopoverOpen}
        onMouseLeave={handlePopoverClose}
      >
        <Link to="#"> {t("versions."+props.element.name)} </Link>
      </Typography>
      <Popover
        id="mouse-over-popover"
        className={classes.popover}
        classes={{
          paper: classes.paper,
        }}
        open={open}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 1,
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: 1,
          horizontal: "left",
        }}
        onClose={handlePopoverClose}
        disableRestoreFocus
        style={{ pointerEvents: "none", marginTop: "18px" }}
        transitionDuration={{
          enter: 100,
          exit: 0,
        }}
      >
        <Typography >
          <List component="nav" aria-label="secondary mailbox folders">
            {/* <ListItem key={0}>
              <Link to="#" style={{textDecoration:"none", color: "#3f51b5", width: "100%"}}><ListItemText primary={t("more")} /></Link>
            </ListItem> */}
            {props.element.tech.map((el: any, index: number) => {
              return (
                <ListItem key={index+1}>
                  <ListItemText primary={el} />
                </ListItem>
              );
            })}
          </List>
        </Typography>
      </Popover>
    </div>
  );
}
