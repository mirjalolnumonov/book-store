import React from 'react';
import { Link } from "react-router-dom";
import PageNotFound from '../../assets/images/404.jpg'
const NotFound: React.FC = () => {
    return (
       <div style={{textAlign: "center"}}>
        <img src={PageNotFound}  />
        <p style={{textAlign:"center"}}>
          <Link to="/">Go to Home </Link>
        </p>
      </div>
    )
}

export default NotFound;