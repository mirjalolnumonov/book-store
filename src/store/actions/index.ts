export {
    edituser
} from './users';

export {
    editbook
} from './books';

export {
    editcategories
} from './categories';

export {
    decrement
} from './counter';

export {
  editsocial
} from './socials'
export {
    deletItem,
    storeResult
} from './result';