export const INCREMENT = "INCREMENT";
export const DECREMENT = "DECREMENT";
export const ADD = "ADD";
export const DELETE = "DELETE";
export const CHANGE_CATEGORY = "CHANGE_CATEGORY";
export const CHANGE_SOCIAL = "CHANGE_SOCIAL";
export const CHANGE_USER = "CHANGE_USER";
export const CHANGE_BOOK = "CHANGE_BOOK";

