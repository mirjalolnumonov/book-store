import * as actionTypes from './actionTypes';
export const edituser = (user:object) => {
    return {
        type: actionTypes.CHANGE_USER,
        user: user
    }
}