import * as actionTypes from './actionTypes';
export const editbook = (book:object) => {
    return {
        type: actionTypes.CHANGE_BOOK,
        book: book
    }
}