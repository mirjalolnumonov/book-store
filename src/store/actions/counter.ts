import * as actionTypes from './actionTypes';
export const inc = (value:number, val:any) => {
    return {
        type: actionTypes.INCREMENT,
        value: value, 
        val: val
    }
}

// export const increment = (value, val) => {
//     return dispatch => {
//         dispatch(inc(value, val))
//     }
// }

export const decrement = (value:any, val:any) => {
    return {
        type: actionTypes.DECREMENT,
        value: value, 
        val: val
    }
}