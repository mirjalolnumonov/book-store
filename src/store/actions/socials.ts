import * as actionTypes from './actionTypes';
export const editsocial = (social:object) => {
    return {
        type: actionTypes.CHANGE_SOCIAL,
        social: social
    }
}