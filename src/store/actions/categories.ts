import * as actionTypes from './actionTypes';
export const editcategories = (category:object) => {
    return {
        type: actionTypes.CHANGE_CATEGORY,
        category: category
    }
}