
import * as actionTypes from './actionTypes';

export const add = (value:any) => {
    return {
        type: actionTypes.ADD,
        value:value
    }
}

export const storeResult = (value:any) => {
    return (dispatch:any, getState:any) => {
        setTimeout(()=>{
            console.log(getState());
            dispatch(add(value));
        }, 500);
    }
} 

export const delet = (id:any) => {
    return {
        type: actionTypes.DELETE,
        id: id
    }
}

export const deletItem = (id:any) => {
    
    return  (dispatch:any) => {
        setTimeout(()=>{
            dispatch(delet(id));
        })
    }

}