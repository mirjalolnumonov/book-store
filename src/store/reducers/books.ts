import * as actionTypes from "../actions/actionTypes";

function bookReducer(state = { book: {} }, action:any) {
    switch (action.type) {
      case actionTypes.CHANGE_BOOK:
        return { ...state, book: action.book }
      default:
        return state
    }
  }
  export default bookReducer;