import * as actionTypes from "../actions/actionTypes";

function userReducer(state = { user: {} }, action:any) {
    switch (action.type) {
      case actionTypes.CHANGE_USER:
        return { ...state, user: action.user }
      default:
        return state
    }
  }
  export default userReducer;