import * as actionTypes from "../actions/actionTypes";

function counterReducer(state = { value: 0 }, action:any) {
    switch (action.type) {
      case actionTypes.INCREMENT:
        return { ...state, value: action.value + action.val }
      case actionTypes.DECREMENT:
        return { ...state, value: action.value - action.val }
      default:
        return state
    }
  }
  export default counterReducer;