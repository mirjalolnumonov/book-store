import * as actionTypes from "../actions/actionTypes";

function categoryReducer(state = { category: {} }, action:any) {
    switch (action.type) {
      case actionTypes.CHANGE_CATEGORY:
        return { ...state, category: action.category }
      default:
        return state
    }
  }
  export default categoryReducer;