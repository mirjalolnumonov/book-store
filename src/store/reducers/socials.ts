import * as actionTypes from "../actions/actionTypes";

function socialReducer(state = { social: {} }, action:any) {
    switch (action.type) {
      case actionTypes.CHANGE_SOCIAL:
        return { ...state, social: action.social }
      default:
        return state
    }
  }
  export default socialReducer;