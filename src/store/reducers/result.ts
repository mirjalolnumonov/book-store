import * as actionTypes from "../actions/actionTypes";

function resultReducer(state = {result: [] }, action:any) {
    switch (action.type) {
      case actionTypes.ADD:
        return { ...state, result: state.result.concat(action.value) }
      case actionTypes.DELETE:
        return { ...state, result: state.result.filter((el, index)=>index!==action.id) }
      default:
        return state
    }
  }
  export default resultReducer;