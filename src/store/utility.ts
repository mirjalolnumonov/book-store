export const updateObject = (oldOject:object, updatedObject:object) => {
    return {
        ...oldOject,
        ...updatedObject
    }
}