import i18n from "i18next";
import {useTranslation, initReactI18next } from "react-i18next"; 

// languages

import en from "./en.json";
import ru from "./ru.json";

export const resources = {
    en : { translation: en},
    ru : { translation: ru},
};

export const defaultLanguage = localStorage.getItem("lang") || "en";

i18n
.use(initReactI18next)
.init({
    resources,
    fallbackLng: defaultLanguage,
    interpolation : {
        escapeValue: false,
    },
});

export const lang = [
    { value: "en", name: "English"},
    { value: "ru", name: "Russian"},
]

export default i18n;