import ReactDOM from "react-dom";
import Home from "./layouts/Home/Home";
import AdminPanel from './layouts/AdminPanel/AdminPanel';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import ReduxComp from "./layouts/ReduxComp/ReduxComp";
import  * as Routes from './config/routes/routes';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import counterReducer from "./store/reducers/counter";
import resultReducer from "./store/reducers/result";
import { Provider } from "react-redux";
import thunk from 'redux-thunk';
import "./index.module.scss"
import categoryReducer from "./store/reducers/categories";
import socialReducer from "./store/reducers/socials";
import bookReducer from "./store/reducers/books";
import userReducer from "./store/reducers/users";

const rootReducer = combineReducers({
  ctr: counterReducer,
  res: resultReducer,
  ctg: categoryReducer,
  scl: socialReducer,
  buk: bookReducer,
  user: userReducer
});

const logger = (store:any) => {
  return (next:any) => {
    return (action:any) => {
 //     console.log("[Middleware] Dispatching", action);
      const result = next(action);
//      console.log('[Middleware] next state', store.getState());
      return result;
    }
  }
}

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

let store = createStore(rootReducer, composeEnhancers(applyMiddleware(logger, thunk)));

ReactDOM.render(
  <Provider store={store}>
  <Router>
    <Switch>
      <Route path="/redux">
        <ReduxComp />
      </Route>
      <Route path={Routes.DASHBOARD}>
        <AdminPanel />
      </Route>
      <Route path={Routes.ROOT}>
        <Home />
      </Route>
    </Switch>
  </Router>
  </Provider>,
  document.getElementById("root")
);
